package com.palmi;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) throws IOException {
        Path mainDirPath;
        Stream<Path> mainDirStream;

        do {
            mainDirPath = Utility.getPath(Utility.userPath());
        } while (mainDirPath==null);

        mainDirStream = Files.list(mainDirPath);
        System.out.println(Utility.getFilesFoldersCount(mainDirStream));
    }
}
